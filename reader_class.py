import os
import csv
import pickle
import json
'''
Klasa ChangerCsv pobiera sciezke z plikiem, sciezke docelowa pliku, oraz 
wspolrzedne i wartosc.

Funkcja is_path_file_exists sprawdza istnienie pliku, jesli nie, to wyswietli
stosowny komunikat. Jesli tak to zwroci True.

Funkcja  open_file Otwiera plik oraz tworzy zagnierzdzona liste.

Funkcja change_file_data zmienia wartosci pod podanymi wspolrzednymi.

Funkcja is_path_to_exists sprawdza, czy foldery w katalogu roboczym są obecne. 
Jesli nie, to tworzy je i zapisuje plik.

Funkja save_file zapisuje plik pod podana sciezka.

Funkcja base_return dodaje tablice 'list_line' do tablicy self.csv_base 
(wymagane) oraz zlicza wartosci w poszczegolnych tablicach

Za pomoca funkcji endswith() odczytanie/zapisanie z plikow .csv .pickle .json
'''


class ChangerCsv:

    def __init__(self, path_from, path_to, arguments):
        self.path_from = path_from
        self.path_to = path_to
        self.arguments = arguments
        self.csv_base = []
        self.y_line = 0
        self.x_element = 0

    def is_path_file_exists(self):

        is_file_exist = os.path.exists(self.path_from)
        show_dir = os.listdir()
        if is_file_exist:
            return True
        else:
            return print('Blad! W tym folderze znajduja sie '
                         'nastepujace pliki/foldery: {}'.format(show_dir))

    def base_return(self, list_line):
        self.csv_base.append(list_line)
        self.y_line = len(self.csv_base)
        self.x_element = len(list_line)

    def open_file(self):
        with open(self.path_from, 'r') as f:
            if self.path_from.endswith(".csv"):
                reader = csv.reader(f)
                for line in reader:
                    list_line = []
                    for element in line:
                        list_line.append(element.strip())
                    self.base_return(list_line)
                return self.csv_base, self.y_line, self.x_element

            elif self.path_from.endswith(".pickle"):
                with open(self.path_from, 'r') as fso:
                    reader = fso.readlines()
                    for line in reader:
                        if line == '\n':
                            continue
                        line = line.split(',')
                        list_line = []
                        for element in line:
                            list_line.append(element)
                        self.base_return(list_line)
                    return self.csv_base, self.y_line, self.x_element

            elif self.path_from.endswith(".json"):
                with open(self.path_from, encoding='utf-8-sig') as fl:
                    reader = fl.readlines()
                    for line in reader:
                        if line == '\n':
                            continue
                        line = line.split(',')
                        list_line = []
                        for element in line:
                            list_line.append(element)
                        self.base_return(list_line)
                    return self.csv_base, self.y_line, self.x_element

            else:
                return False

    def change_file_data(self):
        idx = 0
        quantity = len(self.arguments)
        while True:

            if idx < quantity:
                y = int(self.arguments[idx])
                x = int(self.arguments[idx + 1])
                value = self.arguments[idx + 2]
                if y >= self.y_line or x >= self.x_element:
                    print('Podano za wysokie wspolrzedne!')
                    break
                else:
                    self.csv_base[y][x] = value
            else:
                break
            idx += 3

    def is_path_to_exists(self):
        if os.path.exists(self.path_to):
            return True
        else:
            folder = os.path.split(self.path_to)[0]
            if os.path.isdir(folder):
                return True
            else:
                os.makedirs(folder)
                return True

    def save_file(self):
        if self.path_to.endswith(".pickle"):
            writer = 'wb'
        else:
            writer = 'w'

        with open(self.path_to, writer) as f:
            if self.path_to.endswith(".csv"):
                writer = csv.writer(f, delimiter=',', quotechar=' ')
                for line in self.csv_base:
                    writer.writerow(line)
            elif self.path_to.endswith(".pickle"):
                pickle.dump(self.csv_base, f)
            elif self.path_to.endswith(".json"):
                json.dump(self.csv_base, f)
            else:
                return False
