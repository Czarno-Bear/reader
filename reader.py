# Import biblioteki sys, oraz klasy ChangerCsv z pliku reader_class.py
import sys
from reader_class import ChangerCsv

# tablica ciagow znakow do zapisu wartosci
arguments = sys.argv[3:]

# Jezeli podano zla ilosc argumentow, to wyswietli blad
a_qua = len(arguments)  # args_quantity
if a_qua % 3 != 0 or a_qua == 0:
    print('Podano zla ilosc argumentow.')
else:

    # sciezka pliku src hurricanes.csv oraz docelowa dst
    path_from = sys.argv[1]  # src
    path_to = sys.argv[2]  # dst

    filepath = ChangerCsv(path_from, path_to, arguments)
    error = "Podano zly typ pliku"
    # sprawdzenie czy jest prawidlowa sciezka pliku
    if filepath.is_path_file_exists():
        if filepath.open_file():
            filepath.change_file_data()
            filepath.is_path_to_exists()
            if filepath.save_file() is False:
                print(error)
        else:
            print("nie ma")
